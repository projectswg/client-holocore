package com.projectswg.holocore.client

enum class ServerConnectionStatus {
	CONNECTING,
	CONNECTED,
	DISCONNECTING,
	DISCONNECTED
}
